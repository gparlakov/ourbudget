package com.parlakovi.petqjoro.ourbudget.fragments;

/**
 * Created by georgi.parlakov on 7.9.2015 г..
 */

import android.app.DatePickerDialog;

import android.app.Dialog;
import android.os.Bundle;
import android.app.DialogFragment;

import java.util.Calendar;

/**
 * Created by georgi on 13-11-12.
 */
public class DatePickerFragment extends DialogFragment{

    public static final String ON_DATE_SET_LISTENER = "ON_DATE_SET_LISTENER";
    public static final String CALENDAR = "CALENDAR";

    private DatePickerDialog.OnDateSetListener mListener = null;
    private Calendar mCalendar = null;

    public DatePickerFragment() {
    }

    public void Init(DatePickerDialog.OnDateSetListener listener, Calendar calendar) throws  Exception {

        if (listener == null){
            throw new Exception("No onDateSetListener provided for date picker dialog");
        }
        mListener = listener;
        mCalendar = calendar != null ? calendar : Calendar.getInstance();
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        int year = mCalendar.get(Calendar.YEAR);
        int month = mCalendar.get(Calendar.MONTH);
        int day = mCalendar.get(Calendar.DAY_OF_MONTH);

        return new DatePickerDialog(getActivity(), mListener, year, month, day);
    }
}
