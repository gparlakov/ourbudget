package com.parlakovi.petqjoro.ourbudget;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.j256.ormlite.dao.Dao;
import com.parlakovi.petqjoro.ourbudget.DBObjects.Expense;
import com.parlakovi.petqjoro.ourbudget.UI.Controllers.ExpenseSpinnerWithAddNewController;
import com.parlakovi.petqjoro.ourbudget.UI.Controllers.UserSpinnerWithAddNewController;
import com.parlakovi.petqjoro.ourbudget.UI.Interfaces.ISaveInstanceStateHandler;
import com.parlakovi.petqjoro.ourbudget.activities.BaseActivity;
import com.parlakovi.petqjoro.ourbudget.fragments.DatePickerFragment;

import java.sql.SQLException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;


public class MainActivity extends BaseActivity implements DatePickerDialog.OnDateSetListener {

    private final static String CHOSEN_USER = "com.parlakovi.petqjoro.ourbuget.CHOSEN_USER";
    public static final int REQUEST_CODE_ADD_EXPENSE_TYPE = 1001;
    public static final String NEWLY_CREATED_EXPENSE_TYPE = "com.parlakovi.petqjoro.ourbuget.NEWLY_CREATED_USER";
    private UserSpinnerWithAddNewController mUserSelectSpinnerController;
    private Collection<ISaveInstanceStateHandler> childrenThatHaveInstanceStateHandlers = new ArrayList<>();

    public static final int REQUEST_CODE_ADD_USER = 1000;
    public final static String NEWLY_CREATED_USER = "com.parlakovi.petqjoro.ourbuget.NEWLY_CREATED_USER";
    private ExpenseSpinnerWithAddNewController mExpenseTypeSpinnerController;
    private boolean mAddExpenseButtonInitialized;
    private Date mSelectedDate;
    private Button mButtonSetDate;

    private UserSpinnerWithAddNewController getUserSelectSpinnerController(){
        return mUserSelectSpinnerController;
    }

    private ExpenseSpinnerWithAddNewController getExpenseTypeSpinnerController(){
        return mExpenseTypeSpinnerController;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            InitUI();
        }
    }

    private void InitUI() {

        // init payer user controller
        if (mUserSelectSpinnerController == null){
            mUserSelectSpinnerController =
                    new UserSpinnerWithAddNewController((Spinner)findViewById(R.id.spinner_payer), this);
            childrenThatHaveInstanceStateHandlers.add(mUserSelectSpinnerController);
        }

        // init expense type spinner
        if (mExpenseTypeSpinnerController == null) {
            mExpenseTypeSpinnerController =
                    new ExpenseSpinnerWithAddNewController((Spinner) findViewById(R.id.spinner_expenseType), this);
            childrenThatHaveInstanceStateHandlers.add(mExpenseTypeSpinnerController);
        }

        // init the date of expense
        if (mSelectedDate == null){
            mSelectedDate = Calendar.getInstance().getTime();
        }

        mButtonSetDate = (Button)findViewById(R.id.button_selectExpenseDate);
        setDateToUI();
        mButtonSetDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doSetDate();
            }
        });

       /* if (mAddExpenseButtonInitialized == false) {*/
            Button addExpenseButton = (Button) findViewById(R.id.button_addExpense);
            final Activity self = this;
            addExpenseButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    // desciption
                    final EditText descriptionEditText =
                            (EditText)findViewById(R.id.editText_description);
                    String description = descriptionEditText.getText().toString();
                    if(description == null || description.trim().length() == 0){
                        Toast.makeText(self,
                                "Description missing",
                                Toast.LENGTH_LONG).show();
                        return;
                    }

                    // amount
                    final EditText amountEditText =
                            (EditText)findViewById(R.id.editText_amount);
                    final String amountText = amountEditText.getText().toString();
                    if(amountText == null || amountText.trim().length() == 0){
                        Toast.makeText(self,
                                "Amount missing",
                                Toast.LENGTH_LONG).show();
                        return;
                    }
                    new AsyncTask<Void, Void, Expense>() {


                        @Override
                        protected Expense doInBackground(Void... params) {

                            Expense newExpense = new Expense();
                            Date now = Calendar.getInstance().getTime();
                            newExpense.setCreateTimeStamp(now);
                            newExpense.setSyncTimeStamp(now);

                            newExpense.setDescription(descriptionEditText.getText().toString());

                            newExpense.setAmount(Double.parseDouble(amountText));

                            newExpense.setExpenseType(mExpenseTypeSpinnerController.GetExpenseType());

                            newExpense.setPayer(mUserSelectSpinnerController.GetSelectedUser());

                            newExpense.setHasEdits(false);

                            Dao<Expense, Integer> daoExpense = null;
                            try {
                                daoExpense = Global.DBHelper.getDao(Expense.class);
                                daoExpense.create(newExpense);

                            } catch (SQLException e) {
                                e.printStackTrace();
                            }
                            return newExpense;
                        }

                        protected void onPostExecute(Expense result) {
                            if (result != null) {
                                descriptionEditText.setText("");
                                amountEditText.setText("");

                                Toast.makeText(self,
                                        "Added expense: " + result.getDescription() + " for " + result.getAmount(),
                                        Toast.LENGTH_LONG).show();
                            }
                        }

                    }.execute();
                }
            });
            /*mAddExpenseButtonInitialized = true;
        }*/
    }

    private void setDateToUI() {
        mButtonSetDate.setText(DateFormat.getDateInstance().format(mSelectedDate));
    }

    private void doSetDate() {
        DatePickerFragment fragment = new DatePickerFragment();

        Calendar cal =  Calendar.getInstance();
        cal.setTime(mSelectedDate);

        try {
            fragment.Init(this, cal);
        } catch (Exception e) {
            e.printStackTrace();
        }

        fragment.show(getFragmentManager(), "Set Date of expense");
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        final Activity activity = this;
        switch (requestCode){
            case REQUEST_CODE_ADD_USER:{
                if (resultCode == RESULT_OK){
                    getUserSelectSpinnerController().OnAddNewSuccess(data, activity);
                }
                else {
                    getUserSelectSpinnerController().OnAddNewCanceled();
                }
                break;
            }
            case REQUEST_CODE_ADD_EXPENSE_TYPE:{
                if (resultCode == RESULT_OK){
                    getExpenseTypeSpinnerController().OnAddNewSuccess(data, activity);
                }
                else {
                    getExpenseTypeSpinnerController().OnAddNewCanceled();
                }
                break;
            }
            default:
                Log.e(Global.Log_Tag, "Result returned from an UNKNOWN request");
                finish();
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean handled = true;
        int id = item.getItemId();

        switch (id){
            case R.id.action_addUser: {
                // call add user
                break;
            }
            case R.id.action_addCategory: {
                // call add category
                break;
            }
            case R.id.action_addSubCategory: {
                // call add subcategory
                break;
            }
            case R.id.action_settings: {

                break;
            }
            default:{
                handled = super.onOptionsItemSelected(item);
            }
        }

        return handled;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        for (Iterator<ISaveInstanceStateHandler> i = childrenThatHaveInstanceStateHandlers.iterator(); i.hasNext();) {
            ISaveInstanceStateHandler item = i.next();
            item.onSaveInstanceState(outState);
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        Calendar cal = Calendar.getInstance();
        cal.set(year, monthOfYear, dayOfMonth);
        mSelectedDate = cal.getTime();
        setDateToUI();
    }
}
